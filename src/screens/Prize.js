
import React, { Fragment } from "react";
import { View, Image, StyleSheet, Dimensions, TouchableHighlight } from "react-native";
import { NavigationActions } from "react-navigation";
import * as Animatable from "react-native-animatable";

import win from "../assets/images/screens/prize/win.png";
import win1 from "../assets/images/screens/prize/win1.png";
import fail from "../assets/images/screens/prize/fail.png";

import hat from "../assets/images/screens/prize/hat.png";
import hatFull from "../assets/images/screens/prize/hatFull.png";

import speaker from "../assets/images/screens/prize/speaker.png";
import speakerFull from "../assets/images/screens/prize/speakerFull.png";

import sixpack from "../assets/images/screens/prize/sixpack.png";
import sixpackFull from "../assets/images/screens/prize/sixpackFull.png";

import sunglasses from "../assets/images/screens/prize/sunglasses.png";
import sunglassesFull from "../assets/images/screens/prize/sunglassesFull.png";

import towel from "../assets/images/screens/prize/towel.png";
import towelFull from "../assets/images/screens/prize/towelFull.png";

import cup from "../assets/images/screens/prize/cup.png";
import cupFull from "../assets/images/screens/prize/cupFull.png";

const { width, height } = Dimensions.get('window');

export default class Prize extends React.Component {

  state = {
    showWin: true,
    showFail: true,
  }
  renderHat = () => (
    <Fragment>
      <Image style={style.image} source={hatFull} />
      <TouchableHighlight
        onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
      >
        <Animatable.Image
          animation="pulse"
          iterationCount={1}
          duration={5000}
          onAnimationEnd={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
          style={style.image}
          source={hat}
        />
      </TouchableHighlight>
    </Fragment>
  );

  renderSpeaker = () => (
    <Fragment>
      <Image style={style.image} source={speakerFull} />
      <TouchableHighlight
        onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
      >
        <Animatable.Image
          animation="pulse"
          iterationCount={1}
          duration={5000}
          onAnimationEnd={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
          style={style.image}
          source={speaker}
        />
      </TouchableHighlight>
    </Fragment>
  );

  renderSixpack = () => (
    <Fragment>
      <Image style={style.image} source={sixpackFull} />
      <TouchableHighlight
        onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
      >
        <Animatable.Image
          animation="pulse"
          iterationCount={1}
          duration={5000}
          onAnimationEnd={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
          style={style.image}
          source={sixpack}
        />
      </TouchableHighlight>
    </Fragment>
  );

  renderSixpack = () => (
    <Fragment>
      <Image style={style.image} source={sixpackFull} />
      <TouchableHighlight
        onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
      >
        <Animatable.Image
          animation="pulse"
          iterationCount={1}
          duration={5000}
          onAnimationEnd={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
          style={style.image}
          source={sixpack}
        />
      </TouchableHighlight>
    </Fragment>
  );

  renderSunglasses = () => (
    <Fragment>
      <Image style={style.image} source={sunglassesFull} />
      <TouchableHighlight
        onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
      >
        <Animatable.Image
          animation="pulse"
          iterationCount={1}
          duration={5000}
          onAnimationEnd={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
          style={style.image}
          source={sunglasses}
        />
      </TouchableHighlight>
    </Fragment>
  );

  renderTowel = () => (
    <Fragment>
      <Image style={style.image} source={towelFull} />
      <TouchableHighlight
        onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
      >
        <Animatable.Image
          animation="pulse"
          iterationCount={1}
          duration={5000}
          onAnimationEnd={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
          style={style.image}
          source={towel}
        />
      </TouchableHighlight>
    </Fragment>
  );

  renderCup = () => (
    <Fragment>
      <Image style={style.image} source={cupFull} />
      <TouchableHighlight
        onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
      >
        <Animatable.Image
          animation="pulse"
          iterationCount={1}
          duration={5000}
          onAnimationEnd={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
          style={style.image}
          source={cup}
        />
      </TouchableHighlight>
    </Fragment>
  );

  renderWin = () => (
    <Animatable.Image
      animation="pulse"
      iterationCount={1}
      onAnimationEnd={() => this.setState({ showWin: false })}
      duration={5000}
      style={style.image}
      source={Math.floor((Math.random() * 1)) ? win : win1}
    />
  );

  renderFail = () => (
    <TouchableHighlight
      style={style.container}
      onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
    >
      <Animatable.Image
        animation="pulse"
        iterationCount={1}
        duration={5000}
        onAnimationEnd={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)}
        style={style.image}
        source={fail}
      />
    </TouchableHighlight>
  );

  renderPrize = (prize) => {
    switch (prize) {
      case 'hat':
        return this.renderHat();
      case 'speaker':
        return this.renderSpeaker();
      case 'sixpack':
        return this.renderSixpack();
      case 'sunglasses':
        return this.renderSunglasses();
      case 'towel':
        return this.renderTowel();
      case 'cup':
        return this.renderCup();

      default:
        return this.renderFail();
    }
  }

  render() {
    const prize = this.props.navigation.getParam('prize');
    return (
      <View style={style.container}>
        {prize && !this.state.showWin && this.renderPrize(prize)}
        {prize && this.state.showWin && this.renderWin()}
        {!prize && this.renderFail()}
        {prize === 'undefined' && this.renderFail()}
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    width,
    height,
  },
  image: {
    width,
    height,
    position: "absolute",
  },
});
