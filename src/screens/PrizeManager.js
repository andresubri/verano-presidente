import React from "react";
import { TextInput, Image, StyleSheet, Dimensions, TouchableHighlight, Text, ScrollView, KeyboardAvoidingView } from "react-native";
import { NavigationActions } from "react-navigation";
import * as Animatable from "react-native-animatable";

import { getPrizes, setPrizes } from "../services/PrizeManager";
import sunglassesImg from "../assets/images/screens/prize-manager/sunglasses.png";
import hatImg from "../assets/images/screens/prize-manager/hat.png";
import sixpackImg from "../assets/images/screens/prize-manager/sixpack.png";
import speakerImg from "../assets/images/screens/prize-manager/speaker.png";
import towelImg from "../assets/images/screens/prize-manager/towel.png";
import cupImg from "../assets/images/screens/prize-manager/cup.png";

const { width, height } = Dimensions.get('window');

export default class PrizeManager extends React.Component {
  state = {
    prizes: {
      hat: '0',
      sunglasses: '0',
      sixpack: '0',
      towel: '0',
      speaker: '0',
      cup: '0',
    },
  }

  async componentDidMount() {
    const prizes = await getPrizes();
    if (prizes !== null) this.setState({ prizes });
  }

  render() {
    const { prizes } = this.state;
    const { sunglasses, hat, sixpack, speaker, towel, cup } = prizes;
    return (
      <KeyboardAvoidingView behavior="padding">
        <ScrollView contentContainerStyle={style.container}>
          <Image style={style.image} source={sunglassesImg} />
          <TextInput
            value={sunglasses}
            keyboardType='numeric'
            onChangeText={(text) => this.setState({ prizes: { ...prizes, sunglasses: text } })}
            style={style.input} />
          <Image style={style.image} source={hatImg} />
          <TextInput
            value={hat}
            keyboardType='numeric'
            onChangeText={(text) => this.setState({ prizes: { ...prizes, hat: text } })}
            style={style.input} />
          <Image style={style.image} source={sixpackImg} />
          <TextInput
            value={sixpack}
            keyboardType='numeric'
            onChangeText={(text) => this.setState({ prizes: { ...prizes, sixpack: text } })}
            style={style.input} />
          <Image style={style.image} source={speakerImg} />
          <TextInput
            value={speaker}
            keyboardType='numeric'
            onChangeText={(text) => this.setState({ prizes: { ...prizes, speaker: text } })}
            style={style.input} />
          <Image style={style.image} source={towelImg} />
          <TextInput
            value={towel}
            keyboardType='numeric'
            onChangeText={(text) => this.setState({ prizes: { ...prizes, towel: text } })}
            style={style.input} />
          <Image style={style.image} source={cupImg} />
          <TextInput
            value={cup}
            keyboardType='numeric'
            onChangeText={(text) => this.setState({ prizes: { ...prizes, cup: text } })}
            style={style.input} />
          <TouchableHighlight
            style={style.button}
            onPress={() => {
              setPrizes(prizes)
              this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Entry' })], 0)
            }}
          >
            <Text>Guardar</Text>
          </TouchableHighlight>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const style = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingVertical: 50,
    backgroundColor: 'rgb(109, 259, 19)'
  },
  image: {
    width: 400,
    height: 300,
    resizeMode: 'contain'
  },
  input: {
    marginVertical: 20,
    backgroundColor: 'white',
    width: 100,
  },
  button: {
    width: 150,
    height: 50,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  }
});
