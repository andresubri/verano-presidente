import React from "react";
import { View, Image, StyleSheet, Dimensions, TouchableHighlight, Text } from "react-native";
import * as Animatable from "react-native-animatable";
import { NavigationActions } from "react-navigation";
import { BarIndicator } from 'react-native-indicators';
import { jingle } from '../services/Sound';
import { givePrize } from "../services/PrizeManager";
import background from "../assets/images/screens/analyze/background.png";
import title from "../assets/images/screens/analyze/title.png";
import crown from "../assets/images/screens/analyze/crown.png";

const { width, height } = Dimensions.get('window');

export default class Analyze extends React.Component {  
  
  state = {
    randomTemperature: '0',
  }

  async componentDidMount() {
    const prize = await givePrize();
    setTimeout(() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Prize', params: { prize } })], 0), 6000)
    this.randomTemperature();

    jingle.stop();
    jingle.play();
  }
  
  randomTemperature = () => {
    const temperatures = ['1', '5', '3', '4', '6', '-1', '0', '-2', '-3', '-4'];
    const rnd = Math.floor((Math.random() * temperatures.length));
    this.setState({ randomTemperature: temperatures[rnd]}, () => setTimeout(() => this.randomTemperature(), 200))
  };

  render() {
    return (
      <Animatable.View
        animation="fadeIn"
        duration={2000} style={style.container}>
        <Image style={style.image} source={background} />
        <Image
          style={style.image}
          source={title}
        />
        <Animatable.Image
          animation="flash"
          iterationCount="infinite"
          duration={4000}
          style={style.image}
          source={crown}
          />
          <Text style={style.temperature}>{`${this.state.randomTemperature}°`}</Text>
        <BarIndicator count={40} style={style.indicator} color='rgb(109, 253, 19)' />
      </Animatable.View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    width,
    height,
  },
  image: {
    width,
    height,
    position: "absolute",
  },
  indicator: {
    alignSelf: 'center',
    position: "absolute",
    bottom: 90,
  },
  temperature: {
    fontFamily: 'Midnight Sun DEMO',
    fontSize: 36,
    fontWeight: '700',
    top: 165,
    left: 715,
  },
});
