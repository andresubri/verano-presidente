import React from "react";
import { View, Image, StyleSheet, Dimensions, TouchableHighlight } from "react-native";
import { NavigationActions } from "react-navigation";
import * as Animatable from "react-native-animatable";

import background from "../assets/images/screens/entry/background.png";
import title from "../assets/images/screens/entry/title.png";
import button from "../assets/images/screens/entry/button.png";

const { width, height } = Dimensions.get('window');

export default class Entry extends React.Component {
  render() {
    return (
      <TouchableHighlight
        style={style.container}
        onPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Analyze' })], 0)}
        onLongPress={() => this.props.navigation.reset([NavigationActions.navigate({ routeName: 'PrizeManager' })], 0)}>
        <>
          <Image style={style.image} source={background} />
          <Animatable.Image
            animation="bounce"
            iterationCount="infinite"
            duration={4000}
            style={style.image}
            source={title}
          />
          <Image style={style.image} source={button} />
        </>
      </TouchableHighlight>
    );
  }
}

const style = StyleSheet.create({
  container: {
    width,
    height,
  },
  image: {
    width,
    height,
    position: "absolute",
  }
});
