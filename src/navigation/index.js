import { createStackNavigator, createAppContainer } from "react-navigation";

import Entry from "../screens/Entry";
import Analyze from "../screens/Analyze";
import Prize from "../screens/Prize";
import PrizeManager from "../screens/PrizeManager";

const AppNavigator = createStackNavigator({
    Entry,
    Analyze,
    Prize,
    PrizeManager,
}, {
    defaultNavigationOptions: {
        header: null,
    }
});

export default createAppContainer(AppNavigator);
