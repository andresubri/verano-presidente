import Sound from 'react-native-sound';

// Enable playback in silence mode
Sound.setCategory('Playback');

// See notes below about preloading sounds within initialization code below.
export const jingle = new Sound('jingle.wav', Sound.MAIN_BUNDLE, (error) => {});