import AsyncStorage from '@react-native-community/async-storage';

export const setPrizes = async (prizes) => {
    try {
        await AsyncStorage.setItem('prizes', JSON.stringify(prizes));
    } catch (e) {
        // saving error
    }
}

export const getPrizes = async () => {
    try {
        const value = await AsyncStorage.getItem('prizes')
        if (value !== null) {
            return JSON.parse(value);
        }
        return value;
    } catch (e) {
        // error reading value
    }
}

export const givePrize = async () => {
    try {
        const value = await AsyncStorage.getItem('prizes')
        if (value !== null) {
            const prizes = JSON.parse(value);
            
            const prizesFinal = JSON.parse(value);

            let keys = Object.keys(prizes);

            keys.forEach(key => prizes[key] === "0" ? delete prizes[key] : null);

            keys = Object.keys(prizes);

            const rnd = Math.floor((Math.random() * keys.length)) + 1;
            
            if (rnd > keys.length) return null;

            if (prizes[keys[rnd]] !== "0") {
                prizesFinal[keys[rnd]] = JSON.stringify(parseInt(prizesFinal[keys[rnd]]) - 1);
                await setPrizes(prizesFinal);
                return keys[rnd];
            }
        }
    } catch (e) {
        console.log(e)
    }
}
